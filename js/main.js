$(document).ready(function () {
    $(`.tabs li`).click(switchTabs);
    function switchTabs() {
        $(this).addClass(`active`).siblings().removeClass(`active`);
        const tabIndex = $(this).index();
        $(`.content`).removeClass(`active`).eq(tabIndex).addClass(`active`);
    }
});